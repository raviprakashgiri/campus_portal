from django import template
register = template.Library()
 
@register.filter(name='add_attributes')
def add_attributes(field, css):
    attrs = {}
    definition = css.split(',')
 
    for d in definition:
        if ':' not in d:
            attrs['class'] = d
        else:
            t, v = d.split(':')
            attrs[t] = v
 
    return field.as_widget(attrs=attrs)


@register.filter(name='get_nic')
def get_nic(fullname):
	if fullname.upper() == 'DESIGN AND ANALYSIS OF ALGORITHM':
		return 'DAA'
	words = fullname.split()
	if len(words) > 1:
		nic = ""
		for word in words:
			word = word.upper()
			if not word in ['AND','&']:
				nic = nic + word[0]
		return nic
	else:
		return fullname

@register.filter(name='expansion')
def expansion(code):
    expanded = 'OTHER'
    if code == 'CSE':
        expanded = "Computer Science & Engineering"
    if code == "ECE":
        expanded = "Electronics & Communication Engineering"
    if code == "MEC":
        expanded = "Mechanical Engineering"
    if code == "IBT":
        expanded = "Industrial Biotechnology"
    return expanded